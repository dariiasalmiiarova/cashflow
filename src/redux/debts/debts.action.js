let nextId = 0;

export const addDebts = (num, whom) => ({
    type:'ADD_DEBTS',
    id: nextId++,
    num,
    whom
})

export const addArrayDebts = (debts) => ({
    type:'ADD_DEBTS_ARRAY',
    payload: debts,
}) 

export const removeDebts = (id) => ({
    type: 'REMOVE_DETS',
    id
})