// const INITIAL_STATE = {
//     debts: []
// };

export const debtsReducer = (state = [], action) => {
    switch(action.type){
        case 'ADD_DEBTS':
            return[
               ...state,
                {
                    id: action.id,
                    num: action.num,
                    whom: action.whom,
                }
            ]
        case 'ADD_DEBTS_ARRAY':
            return action.payload
        case 'REMOVE_DETS':
            return state.filter((item) => item.id !== action.id) 
        default:
            return state;
    }
}