
export const addBusiness = (key, incomes, investments) => ({
    type: 'ADD_BUSINESS',
    key: key,
    incomes: incomes,
    investments: investments
});
export const addBusinessArray = (business) => ({
    type:'ADD_BUSINESS_ARRAY',
    payload: business,
})
export const deleteBusiness = (id) => ({
    type: 'DELETE_BUSINESS',
    payload: id
})