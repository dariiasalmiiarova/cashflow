import uuid from 'uuid/v1';

export const addBusinessReducer = (state = [], action) => {
    switch(action.type){
        case 'ADD_BUSINESS':
            return [
                ...state,
                    {
                        id: uuid(),
                        key: action.key,
                        incomes: action.incomes,
                        investments: action.investments
                    }
            ]
        case 'ADD_BUSINESS_ARRAY':
            return action.payload
        case 'DELETE_BUSINESS':
            return state.filter((item) => item.id !== action.payload)
        default:
            return state;
    }
}
