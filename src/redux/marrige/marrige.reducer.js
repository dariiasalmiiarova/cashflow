
export const marrigeReducer = (state = { marrige: false }, action) => {
    switch(action.type){
        case 'ADD_MARRIGE':
            return {
                ...state,
                marrige: true
            }
        case 'ADD_MARRIGE_ARRAY':
            return action.payload
        case 'REMOVE_MARRIGE':
            return {
                ...state,
                marrige: false
            }
        default:
            return state;
    }
}