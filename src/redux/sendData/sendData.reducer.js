const INITIAL_STATE = {
    openStatus: false
}
export const SendDataReducer = (state=INITIAL_STATE, action) => {
    switch(action.type){
        case 'OPEN_SEND_DATA':
            return { openStatus: true }
        case 'CLOSE_SEND_DATA':
            return { openStaus: false }
        default:
            return state
    }
}