export const localeAction = (locale) => ({
    type: 'Locale',
    payload: locale
})