export  const localeReducer = (state={}, action) => {
    switch(action.type){
        case "Locale":
            return action.payload
        default:
            return state
    }
}