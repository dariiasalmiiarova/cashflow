// import uuid from 'uuid/v1';
let ID = 0;
export const addRich = () => ({
    type: 'ADD_RICH',
    id: ID++,
})

export const addRichesArray = (riches) => ({
    type:'ADD_RICHES_ARRAY',
    payload: riches
})
export const removeRich = (id) => ({
    type: 'REMOVE_RICH',
    id
})