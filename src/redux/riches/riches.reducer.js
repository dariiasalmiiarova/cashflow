export const richesReducer = (state=[], action) => {
    switch(action.type){
        case 'ADD_RICH':
            return[
                ...state,
                {
                    id: action.id,
                    // count: +state.count + 1
                }
            ]
        case 'ADD_RICHES_ARRAY':
            return action.payload
        case 'REMOVE_RICH':
            return state.filter((item) => item.id !== action.id)
        default:
            return state;
    }
}