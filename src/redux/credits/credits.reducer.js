// const INITIAL_STATE = { 
//     credits: []
// }

export const creditsReducer = (state = [], action) => {
    switch(action.type){
        case 'ADD_CREDIT':
            return[
                ...state,
                {
                    id: action.id,
                    sum: action.sum,
                    payment: action.payment,
                    pass: action.pass,
                }
            ]
        case 'ADD_CREDITS_ARRAY':
            return action.payload
        case 'REMOVE_CREDIT':
            return state.filter((item) => item.id !== action.id)
        default:
            return state;
    }
}

