let nextID = 0;

export const addCredit = (sum, payment) => ({
    type: 'ADD_CREDIT',
    id: nextID++,
    sum: sum,
    payment: payment,
    pass: Math.round(sum / payment),
})

export const addCreditsArray = (credits) => ({
    type:'ADD_CREDITS_ARRAY',
    payload: credits,
})

export const removeCredit = (id) => ({
    type: 'REMOVE_CREDIT',
    id
})