import { combineReducers } from 'redux';
// import { persistReducer } from 'redux-persist';
// import storageSession from 'redux-persist/lib/storage/session' //use local storage

import profReducer from './profession/prof.reducer';
import gameDataReducer from './gameData/gamedata.reducer';
import {addSharesReducer} from './shares/shares.reducer';
import {addBusinessReducer} from './business/business.reducer';
import {creditsReducer} from './credits/credits.reducer';
import {debtsReducer } from './debts/debts.reducer';
import { richesReducer } from './riches/riches.reducer';
import { SendDataReducer } from './sendData/sendData.reducer';
import { localeReducer } from './locale/locale.reducer';
import { marrigeReducer } from './marrige/marrige.reducer';
// const persistConfig = {
//     key: 'root',
//     storage: storageSession,
//     whitelist: ['gameData', 'name', 'allIncome', 'shares', 'business']
// }

const rootReducer = combineReducers({
    profession: profReducer,
    gameData: gameDataReducer,
    shares: addSharesReducer,
    business: addBusinessReducer,
    credits: creditsReducer,
    debts: debtsReducer,
    riches: richesReducer,
    send: SendDataReducer,
    local: localeReducer,
    marrige: marrigeReducer,
});

// export default persistReducer(persistConfig, rootReducer);
export default rootReducer;