import { createStore, applyMiddleware } from 'redux';
// import { persistStore } from 'redux-persist';
import logger from 'redux-logger';

import rootReduser from './root.reducer';

const middlewares = [];

if(process.env.NODE_ENV === 'development'){
    middlewares.push(logger);
}

export const store = createStore(rootReduser, applyMiddleware(...middlewares));

// export const persistor = persistStore(store);

// export default { store, persistor };
export default store;