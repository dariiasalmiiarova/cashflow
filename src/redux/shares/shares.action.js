export const addShare = (key, quantity, price) => ({
    type: 'ADD_SHARE',
    key,
    quantity,
    price
})
export const addSharesArray = (shares) => ({
    type:'ADD_SHARES_ARRAY',
    payload: shares,
})
export const deleteShare = (id) => ({
    type: 'DELETE_SHARE',
    payload: id
})
