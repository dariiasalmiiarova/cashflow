// const INITIAL_STATE = {
//     shares: []
// }
import uuid from 'uuid/v1';
export const addSharesReducer = (state = [], action) => {
    switch(action.type){
        case 'ADD_SHARE':
            return [
                ...state,
                {
                    id: uuid(),
                    key: action.key,
                    quantity: action.quantity,
                    price: action.price
                }
            ]
        case 'ADD_SHARES_ARRAY':
            return action.payload
        case 'DELETE_SHARE':
            return state.filter((item) => item.id !== action.payload)
        default:
            return state;
    }
}
