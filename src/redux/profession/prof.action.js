
export const updateProf = (collectionsMap) => ({
    type: 'UPDATE_PROF_DATA',
    payload: collectionsMap
});