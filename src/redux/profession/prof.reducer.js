// import ProfActionTypes from './prof.types';

const INITIAL_STATE = {
    profession: null
};

const profReducer = (state = INITIAL_STATE, action) => {
    switch(action.type){
        case 'UPDATE_PROF_DATA':
            return {
                ...state,
                profession: action.payload
            }
        default:
            return state
    }
};

export default profReducer;