import { removeCareExpense, removeFlateExpense } from './gameData.utils';

const INITIAL_STATE = {
  game_id: 0,
  name: "",
  profession: "",
  active_incomes: 0,
  passive_incomes: 0,
  rental_housting: 0,
  food_costs: 0,
  clothing_costs: 0,
  travel_expenses: 0,
  phone_costs: 0,
  car_expense: {
    quantity: 0,
    expense: 600,
    sum: 0,
  },
  flat_expense: {
    quantity: 0,
    expense: 200,
    sum: 0,
  },
  cottage_expense: {
    quantity: 0,
    expense: 1000,
    sum: 0,
  },
  yacht_expense: {
    quantity: 0,
    expense: 1500,
    sum: 0,
  },
  plane_expense: {
    quantity: 0,
    expense: 5000,
    sum: 0,
  },
  child_expense: {
    quantity: 0,
    expense: 200,
    sum: 0,
  },
  all_incomes: 0,
  total_expense: 0,
  cash_flow: 0,
  credits: {
    sum: 0,
  },
  item: {},
};

const gameDataReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case "ADD_GAME_DATA":
      return {
        ...state,
        game_id: action.game_id,
        name: action.name,
        profession: action.profession,
        active_incomes: action.active_incomes,
        passive_incomes: action.passive_incomes,
        rental_housting: action.rental_housting,
        food_costs: action.food_costs,
        clothing_costs: action.clothing_costs,
        travel_expenses: action.travel_expenses,
        phone_costs: action.phone_costs,
        all_incomes: action.active_incomes,
        item: action.item,
        total_expense: action.total_expense,
        cash_flow: action.cash_flow
        // total_expense: action.food_costs + action.clothing_costs + action.travel_expenses + action.phone_costs + action.rental_housting + state.car_expense.sum + state.flat_expense.sum + state.cottage_expense.sum + state.yacht_expense.sum + state.plane_expense.sum + state.child_expense.sum,
        // cash_flow: action.cash_flow,
      };
    case "UPDATE_TOTAL_EXPENSE":
      return{
        ...state,
        total_expense: state.food_costs + state.clothing_costs + state.travel_expenses + state.phone_costs + state.rental_housting + state.car_expense.sum + state.flat_expense.sum + state.cottage_expense.sum + state.yacht_expense.sum + state.plane_expense.sum + state.child_expense.sum + state.credits.sum,
      };

    case "UPDATE_CASH_FLOW":
      return{
        ...state,
        cash_flow: (state.passive_incomes + state.active_incomes) - state.total_expense
      }
    case "ADD_INCOMES":
      return {
        ...state,
        passive_incomes: Number(state.passive_incomes) + Number(action.payload),
        all_incomes: Number(state.all_incomes) + Number(action.payload),
      };
    case "DELETE_INCOMES":
      return {
        ...state,
        passive_incomes: Number(state.passive_incomes) - Number(action.payload),
        all_incomes: Number(state.all_incomes) - Number(action.payload),
      };
    case 'QUIT_JOB':
      return{
        ...state,
        active_incomes: 0,
      }
    case 'ADD_CREDIT_SUM':
      return{
        ...state,
        credits: {
          sum: state.credits.sum + Number(action.sum),
        }
      };
    
    case 'REMOVE_CREDIT_SUM': 
      return{
        ...state,
        credits: {
          sum: state.credits.sum - Number(action.sum)
        }
      }
    
    case "ADD_CHILD":
      return {
        ...state,
        child_expense: {
          ...state.child_expense,
          quantity: action.quantity + state.child_expense.quantity,
          sum: action.sum + state.child_expense.sum,
        },
        // total_expense: state.total_expense + state.child_expense.sum,
        // cash_flow: state.cash_flow - state.child_expense.expense,
      };
    // case 'ADD_MARRIGE': 
    //   return {
    //     ...state,
    //     marrige: true
    //   }
    case "ADD_CAR_EXPENSE":
      return {
        ...state,
        car_expense: {
          ...state.car_expense,
          quantity: state.car_expense.quantity + action.quantity,
          sum: state.car_expense.sum + action.sum
        },
        // total_expense: state.total_expense - state.travel_expenses + action.sum,
        // cash_flow: state.cash_flow - action.sum,
        travel_expenses: 0,
      };
    case "ADD_FLAT_EXPENSE":
      return {
        ...state,
        flat_expense: {
          ...state.flat_expense,
          quantity: action.quantity + state.flat_expense.quantity,
          sum: state.flat_expense.sum + action.sum,
        },
        rental_housting: 0,
        // total_expense: state.total_expense + state.flat_expense.sum,
        // cash_flow: state.cash_flow - action.sum,
      };
    case "ADD_COTTAGE_EXPENSE":
      return {
        ...state,
        cottage_expense: {
          ...state.cottage_expense,
          quantity: action.quantity + state.cottage_expense.quantity,
          sum: state.cottage_expense.sum + action.sum,
        },
        // total_expense: state.total_expense + state.cottage_expense.sum,
        // cash_flow: state.cash_flow - state.cottage_expense.sum,
      };
    case "ADD_YACHT_EXPENSE":
      return {
        ...state,
        yacht_expense: {
          ...state.yacht_expense,
          quantity: action.quantity + state.yacht_expense.quantity,
          sum: state.yacht_expense.sum + action.sum,
        },
        // total_expense: state.total_expense + state.cottage_expense.sum,
        // cash_flow: state.cash_flow - state.cottage_expense.expense,
      };
    case "ADD_PLANE_EXPENSE":
      return {
        ...state,
        plane_expense: {
          ...state.plane_expense,
          quantity: action.quantity + state.plane_expense.quantity,
          sum: action.sum + state.plane_expense.sum,
        },
        // total_expense: state.total_expense + state.plane_expense.sum,
        // cash_flow: state.cash_flow - state.plane_expense.expense,
      };
    case "ADD_DEFAULT_DATA":
      return {
        ...state,
        item: action.payload,
      };
    case 'REMOVE_CAR_EXPENSE':
      return{
        ...state,
        car_expense: {
          ...state.car_expense,
          quantity: state.car_expense.quantity - 1,
          sum: state.car_expense.sum - 600,
        },
        travel_expenses: removeCareExpense(state),
        // cash_flow: state.cash_flow + 600 - state.travel_expenses,
        // total_expense: state.total_expense - 600 + state.travel_expenses,
      }
    case 'REMOVE_FLAT_EXPENSE':
      return{
        ...state,
        flat_expense: {
          ...state.flat_expense,
          quantity: state.flat_expense.quantity - 1,
          sum: state.flat_expense.sum - 200,
        },
        // cash_flow: state.cash_flow + 200,
        rental_housting: removeFlateExpense(state),
      }
      case 'REMOVE_COTTAGE_EXPENSE':
        return{
          ...state,
          cottage_expense: {
            ...state.cottage_expense,
            quantity: state.cottage_expense.quantity - 1,
            sum: state.cottage_expense.sum - 1000,
          },
          // total_expense: state.total_expense - 1000,
          // cash_flow: state.cash_flow + 1000,
        }
      case 'REMOVE_YACHT_EXPENSE':
        return{
          ...state,
          yacht_expense: {
            ...state.yacht_expense,
            quantity: state.yacht_expense.quantity - 1,
            sum: state.yacht_expense.sum - 1500,
          },
          // total_expense: state.total_expense - 1500,
          // cash_flow: state.cash_flow + 1500,       
        }
      case 'REMOVE_PLANE_EXPENSE':
        return{
          ...state,
          plane_expense: {
            ...state.plane_expense,
            quantity: state.plane_expense.quantity - 1,
            sum: state.plane_expense.sum - 5000,
          },
          // total_expense: state.total_expense - 5000,
          // cash_flow: state.cash_flow + 5000,
        }
      case 'REMOVE_CHILD_EXPENSE':
        return{
          ...state,
          child_expense: {
            ...state.child_expense,
            quantity: state.child_expense.quantity - 1,
            sum: state.child_expense.sum - 300,
          },
          // total_expense: state.total_expense - 300,
          // cash_flow: state.cash_flow + 300,
        }
      case "REMOVE_MARRIGE": 
        return{
          ...state,
          marrige: {
            quantity: 0
          }
        }
      // case 'LOAD_TO_DB':
      //   return moveToDb();
    default:
      return state;
  }
};

export default gameDataReducer;
