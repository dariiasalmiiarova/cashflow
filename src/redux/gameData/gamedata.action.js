export const addGameData = (
  game_id,
  name,
  profession,
  active_incomes,
  passive_incomes,
  rental_housting,
  food_costs,
  clothing_costs,
  travel_expenses,
  phone_costs,
  total_expense,
  cash_flow,
  item,
) => ({
  type: "ADD_GAME_DATA",
  game_id: game_id,
  name: name,
  profession: profession,
  active_incomes: active_incomes,
  passive_incomes: passive_incomes,
  rental_housting: rental_housting,
  food_costs: food_costs,
  clothing_costs: clothing_costs,
  travel_expenses: travel_expenses,
  phone_costs: phone_costs,
  total_expense: total_expense,
  cash_flow: cash_flow,
  item: item,
});

export const addIncomes = (incomes) => ({
  type: "ADD_INCOMES",
  payload: incomes,
});

export const deleteIncomes = (incomes) => ({
  type: "DELETE_INCOMES",
  payload: incomes,
});

export const quitJob = () => ({
  type: 'QUIT_JOB',
})

export const addCarExpense = (quantity) => ({
  type: "ADD_CAR_EXPENSE",
  quantity,
  sum: 600 * quantity
});

export const addFlatExpense = (quantity) => ({
  type: "ADD_FLAT_EXPENSE",
  quantity,
  sum: 200 * quantity
});

export const addCottageExpense = (quantity) => ({
  type: "ADD_COTTAGE_EXPENSE",
  quantity,
  sum: 1000 * quantity
});

export const addYachtExpense = (quantity) => ({
  type: "ADD_YACHT_EXPENSE",
  quantity,
  sum: 1500 * quantity
});

export const addPlaneExpense = (quantity) => ({
  type: "ADD_PLANE_EXPENSE",
  quantity,
  sum: 5000 * quantity
});

export const addChild = (quantity) => ({
  type: "ADD_CHILD",
  quantity,
  sum: 300 * quantity
});

// export const addMarrige = () => ({
//   type: "ADD_MARRIGE"
// });

export const addDefaultData = (item) => ({
  type: "ADD_DEFAULT_DATA",
  payload: item,
});

export const updateTotalExpense = () => ({
  type: "UPDATE_TOTAL_EXPENSE",
});

export const updateCashFlow = () => ({
  type: 'UPDATE_CASH_FLOW',
})

export const removeCarExpense = () => ({
  type: 'REMOVE_CAR_EXPENSE'
})

export const removeFlatExpense = () => ({
  type: 'REMOVE_FLAT_EXPENSE'
})

export const removeCottageExpense = () => ({
  type: 'REMOVE_COTTAGE_EXPENSE'
})

export const removeYachtExpense = () => ({
  type: 'REMOVE_YACHT_EXPENSE'
})

export const removePlaneExpense = () => ({
  type: 'REMOVE_PLANE_EXPENSE'
})

export const removeChild = () => ({
  type: 'REMOVE_CHILD_EXPENSE'
})

export const removeMarrige = () => ({
  type: 'REMOVE_MARRIGE'
})

export const addCreditSum = (sum) => ({
    type: 'ADD_CREDIT_SUM',
    sum,
})

export const removeCreditSum = (sum) => ({
    type: 'REMOVE_CREDIT_SUM',
    sum
})

export const loadToDb = () => ({
  type: 'LOAD_TO_DB'
})