import React from 'react';
import { connect } from "react-redux";

import { removePlaneExpense, updateTotalExpense, updateCashFlow } from '../../redux/gameData/gamedata.action';

import Checkbox from '@material-ui/core/Checkbox';
import { FormattedMessage } from 'react-intl';

const PlaneChecks = (props) => {
    const handleChange = () => {
        props.removePlaneExpense(props.gameData.plane_expense.quantity);
        props.updateTotalExpense();
        props.updateCashFlow();
    }
    return(
        <div className="richescheck-main">
            <p className="expense-title">
                <FormattedMessage  
                    id="plane-title"
                    defaultMessage="Samoloty" /></p>
            {
                Array.from(Array( props.gameData.plane_expense.quantity)).map((x, index) =>
                    <Checkbox
                        key={index}
                        checked = {true}
                        onChange = {handleChange}
                        color="primary"
                        inputProps={{ 'aria-label': 'primary checkbox' }}
                    />
                )
            }
        </div>
    )
}

const mapDispatchToProps = (dispatch) => ({
    removePlaneExpense: () => dispatch(removePlaneExpense()),
    updateTotalExpense: () => dispatch(updateTotalExpense()),
    updateCashFlow: () => dispatch(updateCashFlow()),
})

const mapStateToProps = (state) => ({
    gameData: state.gameData
})
 
export default connect(mapStateToProps, mapDispatchToProps)(PlaneChecks);