import React from 'react';
import { connect } from "react-redux";

import { removeCarExpense, updateTotalExpense, updateCashFlow } from '../../redux/gameData/gamedata.action';

import Checkbox from '@material-ui/core/Checkbox';
import { FormattedMessage } from 'react-intl';


const CarChecks = (props) => {
    const handleChange = () => {
        props.removeCarExpense();
        props.updateTotalExpense();
        props.updateCashFlow();
    }
    return(
        <div className="richescheck-main">
            <p className="expense-title">
                <FormattedMessage  
                    id="cars-title"
                    defaultMessage="Samochody" /></p>
            {
                Array.from(Array( props.gameData.car_expense.quantity)).map((x, index) =>
                    <Checkbox
                        key={index}
                        checked = {true}
                        onChange = {handleChange}
                        color="primary"
                        inputProps={{ 'aria-label': 'primary checkbox' }}
                    />
                )
            }
        </div>
    )
}

const mapDispatchToProps = (dispatch) => ({
    removeCarExpense: () => dispatch(removeCarExpense()),
    updateTotalExpense: () => dispatch(updateTotalExpense()),
    updateCashFlow: () => dispatch(updateCashFlow()),
})

const mapStateToProps = (state) => ({
    gameData: state.gameData
})
 
export default connect(mapStateToProps, mapDispatchToProps)(CarChecks);