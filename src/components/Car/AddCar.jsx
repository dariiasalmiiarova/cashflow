import React from 'react';
import { connect } from "react-redux";
import { FormattedMessage } from 'react-intl';
import TextField from '@material-ui/core/TextField';

import { addCarExpense, updateTotalExpense, updateCashFlow } from '../../redux/gameData/gamedata.action';

class AddCar extends React.Component {
    state = {
        inpVal: ''
    }
    handleS = (e) => {
        e.preventDefault();
        this.setState({ [e.target.name]: e.target.value })
    }
    handleSubmit = (e) => {
        e.preventDefault();
        try{
            this.props.addCarExpense(Number(this.state.inpVal));
            this.props.updateTotalExpense();
            this.props.updateCashFlow();
            this.setState({ inpVal: '' })
        }
        catch (error) {
            console.log(error);
        }
    }
    render(){
        return (
            <form
                className="form-inline form-rich"
                style={{ width: 100 + "%" }}
                onSubmit={ this.handleSubmit }
            >
                <TextField 
                    id="outlined-basic01" 
                    label={
                        <FormattedMessage  
                            id="cars-quantity-input"
                            defaultMessage="Dodaj samochód" />
                    } 
                    value={this.state.inpVal}
                    name='inpVal'
                    type="number"
                    onChange={this.handleS}
                    variant='outlined'
                    margin='normal'
                />
                <button type="submit" className="btn btn-prima btn-rich">
                <FormattedMessage  
                        id="btn-game-add"
                        defaultMessage="Dodaj" />
                </button>
            </form>
        )
    }
}

const mapDispatchToProps = (dispatch) => ({
    addCarExpense: (quantity) => dispatch(addCarExpense(quantity)),
    updateTotalExpense: () => dispatch(updateTotalExpense()),
    updateCashFlow: () => dispatch(updateCashFlow()),
});

export default connect(null, mapDispatchToProps)(AddCar);