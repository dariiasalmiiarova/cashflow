import React, { Fragment } from 'react';

import { connect } from 'react-redux';
import { removeCredit } from '../../redux/credits/credits.action';
import { updateCashFlow, updateTotalExpense, removeCreditSum } from '../../redux/gameData/gamedata.action';

import { FormattedMessage } from 'react-intl';

const CreditList = ({ credits, removeCredit, updateCashFlow, updateTotalExpense, removeCreditSum }) => {
    const element = credits.map(item => {
        // let actual = "p-list";
        return(
            <div className="table-responsive-md" key={item.key}>
                <table className="table">
                    <thead>
                        <tr>
                            <th scope="col">
                                <FormattedMessage  
                                    id="credits-sum-title"
                                    defaultMessage="Kwota" /></th>
                            <th scope="col">
                                <FormattedMessage  
                                    id="credits-payment-title"
                                    defaultMessage="Płatność" /></th>
                            <th scope="col">
                                <FormattedMessage  
                                    id="credits-pass-title"
                                    defaultMessage="Ile płatności" /></th>        
                            <th scope="col">
                                <FormattedMessage  
                                    id="btn-delete-game"
                                    defaultMessage="Usuń" /></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{ item.payment }</td>
                            <td>{ item.sum } </td>
                            <td>{ item.pass }</td>
                            <td> 
                                <div className="btn-group" role="group" aria-label="Basic example">
                                 <button type="button" className="btn btn-danger" onClick={
                                    () => { removeCredit(item.id); removeCreditSum(+item.payment); updateTotalExpense(); updateCashFlow() } }><i className="fas fa-trash"></i></button>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        )
    })
    if (credits.length === 0){
        return (
            <div className="empty-div">
                <p className="empty-text">
                    <FormattedMessage  
                        id="credits-title-p"
                        defaultMessage="Brak kredytów" /></p>
            </div>
        )
    } else{
        return(
            <div className="todo-list-item-main">
                <Fragment>
                    { element }
                </Fragment>
            </div>
        ) 
    }
}

const mapDispatchToProps = (dispatch) => ({
    removeCredit: (id) => dispatch(removeCredit(id)),
    removeCreditSum: (sum) => dispatch(removeCreditSum(sum)),
    updateTotalExpense: () => dispatch(updateTotalExpense()),
    updateCashFlow:() => dispatch(updateCashFlow()), 
})

const mapStateToProps = (state) => ({
    credits: state.credits
})

export default connect(mapStateToProps, mapDispatchToProps)(CreditList);