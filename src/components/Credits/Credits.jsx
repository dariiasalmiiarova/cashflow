import React from 'react';

import { connect } from 'react-redux';
import { addCredit } from '../../redux/credits/credits.action';
import { addCreditSum, updateTotalExpense, updateCashFlow } from '../../redux/gameData/gamedata.action';
import TextField from '@material-ui/core/TextField';
import { FormattedMessage } from 'react-intl';

class Credits extends React.Component {
    state = {
        sum: '',
        payment: ''
    }
    handleS = (e) => {
        e.preventDefault();
        this.setState({ [e.target.name]: e.target.value })
    }
    handleSubmit = (e) => {
        e.preventDefault();
        try{
            this.props.addCredit(this.state.sum, this.state.payment)
            this.props.addCreditSum(Number(this.state.payment))
            this.props.updateTotalExpense()
            this.props.updateCashFlow()
            this.setState({ sum: '', payment: '' })
        }
        catch (error) {
            console.log(error);
        }
    }
    render(){
        return(
            <div className="business-main">
                <form className="form-inline add-values-form-c" onSubmit={ this.handleSubmit }>
                    <div className="form-group-b">
                        <TextField 
                            id="outlined-basic0001" 
                            label={
                                <FormattedMessage  
                                    id="credits-sum"
                                    defaultMessage="Wprowadź kwotę" />
                            } 
                            value={this.state.sum}
                            name='sum'
                            type="text"
                            onChange={this.handleS}
                            variant='outlined'
                            margin='normal'
                        />
                        <TextField 
                            id="outlined-basic001" 
                            label={
                                <FormattedMessage  
                                    id="credits-payment"
                                    defaultMessage="Wprowadź płatność" />
                            } 
                            value={this.state.payment}
                            name='payment'
                            type="text"
                            onChange={this.handleS}
                            variant='outlined'
                            margin='normal'

                            />
                        </div>    
                    <button type="submit" className="btn btn-prima form-button">
                        <FormattedMessage  
                            id="btn-game-add"
                            defaultMessage="Dodaj" />
                    </button>
                </form>
            </div>
        )
    }    
}

const mapDispatchToProps = dispatch => ({
    addCredit: (sum, payment) => dispatch(addCredit(sum, payment)),
    addCreditSum: (sum) => dispatch(addCreditSum(sum)),
    updateTotalExpense: () => dispatch(updateTotalExpense()),
    updateCashFlow: () => dispatch(updateCashFlow()),
})

export default connect(null, mapDispatchToProps)(Credits);