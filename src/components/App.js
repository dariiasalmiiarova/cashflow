import React, { Fragment } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import Home from '../pages/Home';
import StartGame from './StartGame';
import Signup from '../pages/Signup';
import Login from '../pages/Login';
import CreateNewGame from '../pages/CreateNewGame';
import Spinner from './Spniner/Spinner';
import Header from './Header';
import MobileMenu from './MobileMenu';
import { auth } from '../services/firebase';


const PrivateRoute = ({ component: Component, data, ...rest}) => {
  return(
    <Route 
      {...rest}
      render={ (props) => data.name.length !== 0
       ? <Component {...props}/> 
       : <Redirect to={{ pathname: '/', state: {from: props.location} }} />
      }
    />
  )
}

const PublicRoute = ({ component: Component, authenticated, ...rest}) => (
  <Route 
    {...rest}
    render={(props) => authenticated === false
      ? <Component {...props} />
      : <Redirect to="/" />
    }
  />
)

class App extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      authenticated: false,
      loading: true,
    };
  }

  componentDidMount(){
    auth().onAuthStateChanged((user) => {
      if(user) {
        this.setState({
          authenticated: true,
          loading: false,
        });
      } else{
        this.setState({
          authenticated: false,
          loading: false,
        });
      }
    })
  }

  render() {
    return this.state.loading === true ? <Spinner /> : (
      <Fragment>
        <div className="mobile">
          <MobileMenu />
        </div>
        <div className="desktop">
          <Header />
        </div>
        <Switch>
          <Route exact path="/" component={ Home }></Route>
          <PrivateRoute path="/game" data={this.props.gameData} component={ StartGame }></PrivateRoute>
          <Route path="/createnewgame"  component={CreateNewGame}></Route>
          <PublicRoute path="/signup" authenticated={this.state.authenticated} component={Signup}></PublicRoute>
          <PublicRoute path="/login" authenticated={this.state.authenticated} component={Login}></PublicRoute>
        </Switch>  
      </Fragment>
    )
  }
}

const mapStateToProps = (state) =>({
  gameData: state.gameData,
})


export default connect(mapStateToProps)(App);