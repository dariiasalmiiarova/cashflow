import React , { Fragment } from 'react';
import { connect } from 'react-redux';
import { loadToDb } from '../redux/gameData/gamedata.action';
import { openSend } from '../redux/sendData/sendData.action';
import { auth, db } from '../services/firebase';

import { FormattedMessage } from 'react-intl';


const Save = ({ gameData, shares, business, credits, debts, riches, openSend, marrige }) => {

    const handleClick = () => {
        const uid = auth().currentUser.uid;
        const game_id = gameData.game_id;
        db.ref(`all_games/${uid}/${game_id}`)
            .set({ 
                    gameData: gameData,
                    shares: shares,
                    business: business,
                    credits: credits,
                    debts: debts,
                    riches: riches,
                    marrige: marrige,
                    game_id, 
                    uid 
            })
            .then(
                openSend()
            )
    }
        return (
            <Fragment>
                <button className="btn button-link button-game" onClick={() => handleClick()} >
                    <FormattedMessage  
                        id="btn-save-game"
                        defaultMessage="Zapisz grę" /></button>
            </Fragment>

        )
};

const mapStateToProps = (state) => ({
    gameData: state.gameData,
    shares: state.shares,
    business: state.business,
    credits: state.credits,
    debts: state.debts,
    riches: state.riches,
    marrige: state.marrige.marrige,
});

const mapDispatchToProps = (dispatch) => ({
    loadToDb: () => dispatch(loadToDb()),
    openSend: () => dispatch(openSend()),
})


export default connect(mapStateToProps, mapDispatchToProps)(Save);