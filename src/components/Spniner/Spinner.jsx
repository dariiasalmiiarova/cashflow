import React from 'react';
import './spinner.css';

const Spinner = () => {
    return (
        <div className="spinner-main">
            <div className="loadingio-spinner-spinner-4spbdid8p93"><div className="ldio-kgzwfabn17f">
            <div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div>
            </div></div>
        </div>
    )
}

export default Spinner;
