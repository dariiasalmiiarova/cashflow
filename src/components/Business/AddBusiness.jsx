import React from 'react';
import { connect } from 'react-redux';

import { addBusiness } from '../../redux/business/business.action';
import { addIncomes, updateCashFlow } from '../../redux/gameData/gamedata.action';

import { FormattedMessage } from 'react-intl';

import TextField from '@material-ui/core/TextField';

class AddBusiness extends React.Component{
    constructor(){
        super();
        this.state = {
            key: '',
            incomes: '',
            investments: ''
        }
    }
    handleInput = ( e ) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    handleSubmit = e => {
        e.preventDefault();
        try{
            this.props.addBusiness(this.state.key, this.state.incomes, this.state.investments);
            // this.props.updateAllIncomes(this.state.incomes);
            this.props.addIncomes(Number(this.state.incomes));
            this.props.updateCashFlow();
        } catch (error){
            console.log(error);
        }
        this.setState({
            key:'',
            incomes:'',
            investments:''
        })
    }
    render(){
        return(
            <div className="business-main">
                <form className="form-inline add-values-form" style={{width: 100+'%'}} onSubmit={this.handleSubmit} >
                    <div className="form-group-b">
                    <TextField 
                            id="outlined-basic4" 
                            label={
                                <FormattedMessage  
                                id="business-key-select"
                                defaultMessage="Typ" />
                            } 
                            name='key'
                            type='text'
                            onChange={this.handleInput}
                            value={this.state.key}
                            variant='outlined'
                            margin='normal'
                            className='input-form'
                            required 
                            />
                    <TextField 
                            id="outlined-basic5" 
                            label={
                                <FormattedMessage  
                                id="business-income-input"
                                defaultMessage="Wprowadź dochód" />
                            } 
                            name='incomes'
                            type='text'
                            onChange={this.handleInput}
                            value={this.state.incomes}
                            variant='outlined'
                            margin='normal'
                            className='input-form'
                            required 
                            />
                    <TextField 
                        id="outlined-basic6" 
                        label={
                            <FormattedMessage  
                            id="business-envestents-input"
                            defaultMessage="Wprowadź inwestycję" />
                        } 
                        name='investments'
                        type='text'
                        onChange={this.handleInput}
                        value={this.state.investments}
                        variant='outlined'
                        margin='normal'
                        className='input-form'
                        required 
                    />        
                    </div>
                    <button type="submit" className="btn btn-prima form-button">
                        <FormattedMessage  
                                id="btn-game-add"
                                defaultMessage="Dodaj" /></button>
                </form>
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => ({
    addBusiness: (key, incomes, investments) => dispatch(addBusiness(key, incomes, investments)),
    // updateAllIncomes: (incomes) => dispatch(updateAllIncomes(incomes)),
    addIncomes: (incomes) => dispatch(addIncomes(incomes)),
    updateCashFlow: () => dispatch(updateCashFlow()),
})


export default connect(null, mapDispatchToProps)(AddBusiness);