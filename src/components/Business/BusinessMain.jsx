import React from 'react';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';

import AddBusiness from './AddBusiness';
import BusinessItem from './BusinessItem';

const BusinessMain = ({business}) => {
    return(
        <div className="business-main-c">
            <div className="business-head">
                <p className="business-head-content">
                <FormattedMessage  
                        id="business-main-title"
                        defaultMessage="Dochód" /></p>
            </div>
            <div className="business-table">
                <BusinessItem business = {business} />
                <AddBusiness />
            </div>
            
        </div>
    )
}

const mapStateToProps = (state) => ({
    business: state.business
})

export default connect(mapStateToProps)(BusinessMain);