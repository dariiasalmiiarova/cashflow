import React, {Fragment} from 'react'
import { connect } from 'react-redux';

import {deleteBusiness } from '../../redux/business/business.action';
import { deleteIncomes, updateCashFlow} from '../../redux/gameData/gamedata.action';
import { FormattedMessage } from 'react-intl';


const BusinessItem = ({ business, deleteBusiness, deleteIncomes, updateCashFlow }) => {
    const element = business.map(item => {
        
        return(
            <div className="table-responsive-md" key={item.key}>
                <table className="table">
                    <thead>
                        <tr>
                            <th scope="col">
                                <FormattedMessage  
                                    id="business-category-title"
                                    defaultMessage="Kategoria" /></th>
                            <th scope="col">
                                <FormattedMessage  
                                    id="business-income-title"
                                    defaultMessage="Dochód" /></th>
                            <th scope="col">
                                <FormattedMessage  
                                    id="business-investments-title"
                                    defaultMessage="Inwestycje" /></th>
                            <th scope="col">
                                <FormattedMessage  
                                    id="btn-delete-game"
                                    defaultMessage="Usuń" /></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{ item.key }</td>
                            <td>{ item.incomes } </td>
                            <td>{ item.investments }</td>
                            <td> 
                                <div className="btn-group" role="group" aria-label="Basic example">
                                    <button type="button" className="btn btn-danger" onClick={
                                        () => {deleteBusiness(item.id); deleteIncomes(item.incomes); updateCashFlow()} }><i className="fas fa-trash"></i></button>
                                </div> 
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

        )
    })
    if (business.length === 0){
        return (
            <div className="empty-div">
                <p className="empty-text">
                    <FormattedMessage  
                        id="business-empty-p"
                        defaultMessage="Brak pasywnego dochodu" /></p>
            </div>
        )
    } else{
        return(
            <div className="todo-list-item-main">
                <Fragment>
                    { element }
                </Fragment>
            </div>
        ) 
    }
}

const mapDispatchToProps = dispatch => ({
    deleteBusiness: id => dispatch(deleteBusiness(id)),
    deleteIncomes: incomes => dispatch(deleteIncomes(incomes)),
    updateCashFlow: () => dispatch(updateCashFlow()),
})

export default connect(null, mapDispatchToProps)(BusinessItem);