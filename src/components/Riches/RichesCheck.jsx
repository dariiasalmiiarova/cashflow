import React, { Fragment } from 'react';
import { connect } from 'react-redux';

import { removeRich } from '../../redux/riches/riches.action';

import Checkbox from '@material-ui/core/Checkbox';


const RichesCheck = ({ riches, removeRich }) => {
    return(
        <Fragment>
            {
                riches.map((item) => 
                    <Checkbox
                        key={item.id}
                        checked = {true}
                        onChange = {() => removeRich(item.id)}
                        color="primary"
                        inputProps={{ 'aria-label': 'primary checkbox' }}
                    />
                )
            }
        </Fragment>
    )
}

const mapDispatchToProps = (dispatch) => ({
    removeRich: (id) => dispatch(removeRich(id)),
})

const mapStateToProps = (state) => ({
    riches: state.riches
})

export default connect(mapStateToProps, mapDispatchToProps)(RichesCheck);