import React, { Fragment } from 'react';
import { connect } from 'react-redux';

import { addRich } from '../../redux/riches/riches.action';
import { FormattedMessage } from 'react-intl';

const AddRiches = ({ addRich }) => {
    return(
        <Fragment>
            <button type="submit" className="btn btn-prima" onClick={() => addRich() }>
                <FormattedMessage  
                    id="btn-game-add"
                    defaultMessage="Dodaj" /></button> 
        </Fragment>
    )
}

const mapDispatchToProps = (dispatch) => ({
    addRich: () => dispatch(addRich()),
})


export default connect(null, mapDispatchToProps)(AddRiches);