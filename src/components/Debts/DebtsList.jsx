import React, {Fragment} from 'react';
import {connect} from 'react-redux';

import {removeDebts} from '../../redux/debts/debts.action';
import { FormattedMessage } from 'react-intl';

const DebtsList = ({removeDebts, debts}) => {
    const element = debts.map(item => {
        // let actual = "p-list";
        return(
            <div className="table-responsive-md" key={item.key}>
                <table className="table">
                    <thead>
                        <tr>
                            <th scope="col">
                                <FormattedMessage  
                                    id="debts-whom-title"
                                    defaultMessage="Komu" /></th>
                            <th scope="col">
                                <FormattedMessage  
                                    id="debts-sum-title"
                                    defaultMessage="Kwota" /></th>      
                            <th scope="col">
                                <FormattedMessage  
                                    id="btn-delete-game"
                                    defaultMessage="Usuń" /></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{ item.whom }</td>
                            <td>{ item.num } </td>
                            <td> 
                                <div className="btn-group" role="group" aria-label="Basic example">
                                    <button type="button" className="btn btn-danger" onClick={
                                        () => {removeDebts(item.id)} }><i className="fas fa-trash"></i></button>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            // <ul className="todo-list-ul list-group" key={item.id}>
            //     <li className="list-group-item d-flex align-items-center justify-content-between" >
            //         <p className={ actual } > { item.whom } </p> 
            //         <p className={ actual } > { item.num } </p> 
            //         <div className="btn-group" role="group" aria-label="Basic example">
            //             <button type="button" className="btn btn-danger" onClick={
            //                 () => {removeDebts(item.id)} }><i className="fas fa-trash"></i></button>
            //         </div>
            //     </li>
            // </ul>
        )
    })
    if (debts.length === 0){
        return (
            <div className="empty-div">
                <p className="empty-text">
                    <FormattedMessage  
                        id="debts-title-p"
                        defaultMessage="Brak zaległości" /></p>
            </div>
        )
    } else{
        return(
            <div className="todo-list-item-main">
                <Fragment>
                    { element }
                </Fragment>
            </div>
        ) 
    }
}

const mapDispatchToProps = (dispatch) => ({
    removeDebts: (id) => dispatch(removeDebts(id)),
})

const mapStateToProps = (state) => ({
    debts: state.debts
})

export default connect (mapStateToProps, mapDispatchToProps)(DebtsList);