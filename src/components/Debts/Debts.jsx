import React from 'react';
import { connect } from 'react-redux';
import {addDebts} from '../../redux/debts/debts.action';

import TextField from '@material-ui/core/TextField';
import { FormattedMessage } from 'react-intl';

class Debts extends React.Component {
    state = {
        whom: '',
        payment: ''
    }
    handleS = (e) => {
        e.preventDefault();
        this.setState({ [e.target.name]: e.target.value })
    }
    handleSubmit = (e) => {
        e.preventDefault();
        try{
            addDebts(this.state.sum, this.state.whom)
            this.setState({ whom: '',  payment: ''})
        }
        catch (error) {
            console.log(error);
        }
    }
    render(){
        return (
            <div className="business-main">
                <form className="form-inline add-values-form-c" onSubmit={ this.handleSubmit }>
                    <div className="form-group-b">
                        <TextField 
                            id="outlined-basic01" 
                            label={
                                <FormattedMessage  
                                    id="debts-whom"
                                    defaultMessage="Wpisz, od kogo pożyczyłeś" />
                            } 
                            value={this.state.whom}
                            name='sum'
                            type="text"
                            onChange={this.handleS}
                            variant='outlined'
                            margin='normal'
                        />
                        <TextField 
                            id="outlined-basic01" 
                            label={
                                <FormattedMessage  
                                    id="debts-sum"
                                    defaultMessage="Wprowadź kwotę" />
                            } 
                            value={this.state.sum}
                            name='payment'
                            type="text"
                            onChange={this.handleS}
                            variant='outlined'
                            margin='normal'
                            />
                    </div>
                    <button type="submit" className="btn btn-prima form-button">
                        <FormattedMessage  
                            id="btn-game-add"
                            defaultMessage="Dodaj" />
                    </button>
                </form>
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => ({
    addDebts: (num, whom) => dispatch(addDebts(num, whom))
})

export default connect(null, mapDispatchToProps)(Debts)