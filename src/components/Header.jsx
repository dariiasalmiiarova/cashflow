import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { auth } from '../services/firebase';
import { Context } from '../Translation/Translation';
import { FormattedMessage } from 'react-intl';

import russia from '../images/russia.svg'
import poland from '../images/poland.svg';

const Header = () => {
  const context = useContext(Context)
  return(
    <header className="header">
      <nav className="navbar navbar-expand-md">
        <div className="container">  
            <Link className="navbar-brand" to="/">CashFlow</Link>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse justify-content-end" id="navbarNavAltMarkup">
            {auth().currentUser
                ? <div className="navbar-nav">
                    <Link className="btn button-link btn-primary button-header" to="/createnewgame">
                    <FormattedMessage  
                        id="btn-new-game"
                        defaultMessage="Nowa gra" /></Link>
                    <button className="btn button-link button-header" onClick={() => auth().signOut()}>
                    <FormattedMessage  
                        id="btn-logout"
                        defaultMessage="Logout" /></button>
                  </div>
                : <div className="navbar-nav">
                <Link className="btn button-link button-header" to="/login">
                  <FormattedMessage  
                        id="btn-sign-in"
                        defaultMessage="Sign In" />
                </Link>
                <Link className="btn button-link button-header" to="/signup">
                  <FormattedMessage 
                        id="btn-sign-up"
                        defaultMessage="Sign Up" />
                </Link>
                </div>}
            </div>
            <div className="flags-container">
              <div className="flags-box">
                <img alt="Poland flag" className="flag" src={poland} onClick={context.selectLangPolish}></img>
                <img onClick={context.selectLangRussian} alt="Russian flag" className="flag" src={russia}></img>
              </div>
            </div>
        </div>
      </nav>
    </header>
  )
}

export default Header;