import React, { useContext } from 'react'
import { Context } from '../Translation/Translation'

import russia from '../images/russia.svg'
import poland from '../images/poland.svg'

const Flags = ( props ) => {
    const context = useContext(Context)

    return(
        <div className="flags-container">
            <div className="flags-box">
                <img alt="Poland flag" className="flag" src={poland} onClick={context.selectLangPolish}></img>
                <img onClick={context.selectLangRussian} alt="Russian flag" className="flag" src={russia}></img>
            </div>
        </div>
    )
}

export default Flags