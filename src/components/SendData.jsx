import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import heartImg from '../images/heavy-black-heart_2764.png'
import { FormattedMessage } from 'react-intl';

const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

export default function SendMessage(props) {
  const classes = useStyles();

  return (
    <div>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={props.stateInfo || false}
        onClose={props.closeStatus()}
        closeAfterTransition
        BackdropComponent={Backdrop}
      >
        <Fade in={props.stateInfo}>
          <div className={classes.paper + " div-send-message"}>
            <p id="transition-modal-description">
              <FormattedMessage  
                id="send-message"
                defaultMessage="Twoje dane zostawy wysłane!" /></p>
            <img className="heart" src={heartImg} alt="heart"></img>
          </div>
        </Fade>
      </Modal>
    </div>
  );
}
