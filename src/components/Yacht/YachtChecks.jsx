import React from 'react';
import { connect } from "react-redux";

import { removeYachtExpense, updateTotalExpense, updateCashFlow } from '../../redux/gameData/gamedata.action';

import Checkbox from '@material-ui/core/Checkbox';
import { FormattedMessage } from 'react-intl';

const YachChecks = (props) => {
    const handleChange = () => {
        props.removeYachtExpense(props.gameData.yacht_expense.quantity);
        props.updateTotalExpense();
        props.updateCashFlow();
    }
    return(
        <div className="richescheck-main">
            <p className="expense-title">
                <FormattedMessage  
                    id="yacht-title"
                    defaultMessage="Jachty" /></p>
            {
                Array.from(Array( props.gameData.yacht_expense.quantity)).map((x, index) =>
                    <Checkbox
                        key={index}
                        checked = {true}
                        onChange = {handleChange}
                        inputProps={{ 'aria-label': 'primary checkbox' }}
                    />
                )
            }
        </div>
    )
}

const mapDispatchToProps = (dispatch) => ({
    removeYachtExpense: () => dispatch(removeYachtExpense()),
    updateTotalExpense: () => dispatch(updateTotalExpense()),
    updateCashFlow: () => dispatch(updateCashFlow()),
})

const mapStateToProps = (state) => ({
    gameData: state.gameData
})
 
export default connect(mapStateToProps, mapDispatchToProps)(YachChecks);