import React from "react";
import HamburgerMenu from "react-hamburger-menu";
import { FormattedMessage } from "react-intl";
import { auth } from '../services/firebase';
import Home from '../pages/Home';
import { Link, NavLink } from 'react-router-dom';
import Flags from './Flags';

export default class MobileMenu extends React.Component {
  constructor() {
    super();
    this.state = {
      open: false,
    };
  }
  handleClick() {
    this.setState({
      open: !this.state.open,
    });
  }
  render() {
    let mobileFixedUlClass = "fixed-ul-menu";
    if (this.state.open) {
      mobileFixedUlClass += " active-fixed-mobile-menu";
    }
    return (
      <div className="mobile-menu-fixed">
        <div className="mobile-menu-container-fixed">
          <div className="right-block">
            <HamburgerMenu
              className="hamburger-menu"
              isOpen={this.state.open}
              menuClicked={this.handleClick.bind(this)}
              width={18}
              height={15}
              strokeWidth={1}
              rotate={0}
              color="white"
              borderRadius={0}
              animationDuration={0}
            />
            <Flags />
          </div>
        </div>
          <ul className={mobileFixedUlClass}>
          {auth().currentUser
                ? <div className="navbar-nav mobile-menu-container-fixed">
                    <NavLink className="mobile-menu-item-a" to="/"> <li className="mobile-menu-item-li"> Cashflow </li> </NavLink>
                    <NavLink className="mobile-menu-link-a" to="/createnewgame">
                        <li className="mobile-menu-item-li"><FormattedMessage  
                          id="btn-new-game"
                          defaultMessage="Nowa gra" /></li>
                    </NavLink>
                    <div className="button-logout-div">
                      <button className="MuiButtonBase-root MuiButton-root MuiButton-outlined button-logout" onClick={() => auth().signOut()}>
                        <FormattedMessage  
                            id="btn-logout"
                            defaultMessage="Logout" />
                      </button>
                    </div>
                  </div>
                : <div className="navbar-nav">
                  <div className="button-logout-div">
                    <NavLink className="mobile-menu-item-li button-logout" to="/login">
                      <li className="mobile-menu-item-li"><FormattedMessage  
                            id="btn-sign-in"
                            defaultMessage="Sign In" /></li>
                    </NavLink>
                  </div>
                  <div className="button-logout-div">
                    <NavLink className="mobile-menu-item-li button-logout" to="/signup">
                      <FormattedMessage 
                            id="btn-sign-up"
                            defaultMessage="Sign Up" />
                    </NavLink>
                  </div>
                </div>
            }
          </ul>
      </div>
    );
  }
}
