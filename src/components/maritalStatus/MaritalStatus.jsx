import React, { Fragment } from 'react';
import { connect } from 'react-redux';

import AddMarrige from '../Marrige/AddMarrige';
import MarrigeCheck from '../Marrige/MarrigeCheck';
import AddChild from '../Child/AddChild';
import ChildChecks from '../Child/ChildChecks';
import { FormattedMessage } from 'react-intl';


const MaritalStatus = ({ marrige }) => {
    return(
        <Fragment>
            <div className="marital-main">
                <div className="marrige-div">
                    <p className="marrige expense-title">
                        <FormattedMessage  
                            id="marige-title"
                            defaultMessage="Ślub" /></p>
                    {
                        marrige.marrige === true ? <MarrigeCheck /> : <AddMarrige />
                    }
                    
                </div>
                <div className="children-main-div">
                    <div className="children-div">
                        <p className="children expense-title">
                        <FormattedMessage  
                                id="children-title"
                                defaultMessage="Dzieci" /></p>
                        <ChildChecks />
                    </div>
                    <AddChild />
                </div>
            </div>
        </Fragment>
    )
}
const mapStateToProps = (state) => ({
    marrige: state.marrige
})

export default connect(mapStateToProps)(MaritalStatus);