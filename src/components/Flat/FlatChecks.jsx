import React from 'react';
import { connect } from "react-redux";

import { removeFlatExpense, updateTotalExpense, updateCashFlow } from '../../redux/gameData/gamedata.action';

import Checkbox from '@material-ui/core/Checkbox';
import { FormattedMessage } from 'react-intl';

const FlatChecks = (props) => {
    const handleChange = () => {
        props.removeFlatExpense(props.gameData.flat_expense.quantity);
        props.updateTotalExpense();
        props.updateCashFlow();
    }
    return(
        <div className="richescheck-main">
            <p className="expense-title">
                <FormattedMessage  
                    id="flat-title"
                    defaultMessage="Mieszkanie" /></p>
            {
                Array.from(Array( props.gameData.flat_expense.quantity)).map((x, index) =>
                    <Checkbox
                        key={index}
                        checked = {true}
                        onChange = {handleChange}
                        color="primary"
                        inputProps={{ 'aria-label': 'primary checkbox' }}
                    />
                )
            }
        </div>
    )
}

const mapDispatchToProps = (dispatch) => ({
    removeFlatExpense: () => dispatch(removeFlatExpense()),
    updateTotalExpense: () => dispatch(updateTotalExpense()),
    updateCashFlow: () => dispatch(updateCashFlow()),
})

const mapStateToProps = (state) => ({
    gameData: state.gameData
})
 
export default connect(mapStateToProps, mapDispatchToProps)(FlatChecks);