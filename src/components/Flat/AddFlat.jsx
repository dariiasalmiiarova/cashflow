import React from 'react';
import { connect } from "react-redux";
import { FormattedMessage } from 'react-intl';
import TextField from '@material-ui/core/TextField';
import { addFlatExpense, updateTotalExpense, updateCashFlow } from '../../redux/gameData/gamedata.action';

class AddFlat extends React.Component {
    state = {
        inpVal: ''
    }
    handleS = (e) => {
        e.preventDefault();
        this.setState({ [e.target.name]: e.target.value })
    }
    handleSubmit = (e) => {
        e.preventDefault();
        try{
            this.props.addFlatExpense(Number(this.state.inpVal));
            this.props.updateTotalExpense();
            this.props.updateCashFlow();
            this.setState({ inpVal: '' })
        }
        catch (error) {
            console.log(error);
        }
    }
    render(){
        return (
            <form
                className="form-inline form-rich"
                style={{ width: 100 + "%" }}
                onSubmit={ this.handleSubmit }
            >
               <TextField 
                    id="outlined-basic01" 
                    label={
                        <FormattedMessage  
                            id="flat-quantity-input"
                            defaultMessage="Dodaj mieszkanie" />
                    } 
                    value={this.state.inpVal}
                    name='inpVal'
                    type="number"
                    onChange={this.handleS}
                    variant='outlined'
                    margin='normal'
                />
                <button type="submit" className="btn btn-prima btn-rich">
                    <FormattedMessage  
                        id="btn-game-add"
                        defaultMessage="Dodaj" />
                </button>
            </form>
        )
    }    
}

const mapDispatchToProps = (dispatch) => ({
    addFlatExpense: (quantity) => dispatch(addFlatExpense(quantity)),
    updateTotalExpense: () => dispatch(updateTotalExpense()),
    updateCashFlow: () => dispatch(updateCashFlow()),
});

export default connect(null, mapDispatchToProps)(AddFlat);