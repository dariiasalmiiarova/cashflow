import React from 'react';

import AddCar from './Car/AddCar';
import CarChecks from './Car/CarChecks';
import CottageChecks from './Cottage/CottageChecks';
import AddCottage from './Cottage/AddCottage';
import AddFlat from './Flat/AddFlat';
import FlatChecks from './Flat/FlatChecks';
import YachtChecks from './Yacht/YachtChecks';
import AddYacht from './Yacht/AddYacht';
import AddPlane from './Plane/AddPlane';
import PlaneChecks from './Plane/PlaneChecks';
import MaritalStatus from './maritalStatus/MaritalStatus';
import RichesCheck from './Riches/RichesCheck';
import AddRiches from './Riches/AddRiches';

import { FormattedMessage } from 'react-intl';



const Expenses = () => {
    return (
        <div className="expense-div">
            <div className="expenses-main">
                <div className="expenses-row">
                    <CarChecks />
                    <AddCar />
                    
                </div>
                <div className="expenses-row">
                    <FlatChecks />
                    <AddFlat />
                    
                </div>
                <div className="expenses-row">
                    <CottageChecks />
                    <AddCottage />
                    
                </div>
                <div className="expenses-row">
                    <YachtChecks />
                    <AddYacht />
                    
                </div>
                <div className="expenses-row">
                    <PlaneChecks />
                    <AddPlane />
                    
                </div>
            </div>
            
            <div className="marital">
                <div className="marrige-div riches-div">
                    <p className="expense-title">
                        <FormattedMessage  
                            id="riches-main-title"
                            defaultMessage="Kaprysy" /></p>
                    <div className="riches-tab">
                    <RichesCheck />
                    <AddRiches />
                    </div>
                </div>
                <MaritalStatus />
            </div>
        </div>
    )
}

export default Expenses;