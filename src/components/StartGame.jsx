import React from "react";

import { connect } from "react-redux";
import SharesAdd from "./Shares/SharesAdd";
import SharesItem from './Shares/SharesItem';
import BusinessMain from "./Business/BusinessMain";
import Credits from "./Credits/Credits";
import Debts from "./Debts/Debts";
import Expenses from './Expenses';
import DebtsList from './Debts/DebtsList';
import CreditList from './Credits/CreditList';
// import RichesCheck from './Riches/RichesCheck';
// import AddRiches from './Riches/AddRiches';
import GameData from './GameData';
import SendData from './SendData';

import { closeSend } from '../redux/sendData/sendData.action';

import { FormattedMessage } from 'react-intl';

class StatrGame extends React.Component {
  closePopupStatus = () => {
    if (this.props.send === true) {
      setTimeout(() => {
        this.props.closeSend()
      }, 3000);
    }
  };
  render() {
    const { send } = this.props;
    return (
      <div className="game-main">
        <div className="container">
          <GameData />
          <SendData stateInfo={send} closeStatus={ this.closePopupStatus }  />
          <div className="business">
            <BusinessMain />
          </div>
          <div className="shares business">
            <p className="business-head-content">
              <FormattedMessage  
                id="shares-main-title"
                defaultMessage="Akcje" /></p>
            <div className="business-table">
              <SharesItem />
              <SharesAdd />
            </div>
          </div>

          <div className="riches business">
            <Expenses />
          </div>
          <div className="credits business">
            <p className="business-head-content">
              <FormattedMessage  
                id="credits-main-title"
                defaultMessage="Kredyty" /></p>
                <div className="business-table">
                  <CreditList />
                  <Credits />
                </div>
          </div>
          <div className="debts business">
            <p className="business-head-content">
              <FormattedMessage  
                id="debts-main-title"
                defaultMessage="Zaległości" /></p>
            <div className="business-table">
              <DebtsList />
              <Debts />
            </div>
          </div>
          
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  gameData: state.gameData,
  shares: state.shares,
  business: state.business,
  credits: state.credits,
  debts: state.debts,
  send: state.send.openStatus,
});

const mapDispatchToProps = (dispatch) => ({
  closeSend: () => dispatch(closeSend())
})

export default connect(mapStateToProps, mapDispatchToProps)(StatrGame);
