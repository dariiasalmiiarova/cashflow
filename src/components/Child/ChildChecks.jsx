import React from 'react';
import { connect } from "react-redux";

import { removeChild, updateTotalExpense, updateCashFlow } from '../../redux/gameData/gamedata.action';

import Checkbox from '@material-ui/core/Checkbox';

const ChildChecks = (props) => {
    const handleChange = () => {
        props.removeChild(props.gameData.child_expense.quantity);
        props.updateTotalExpense();
        props.updateCashFlow();
    }
    return(
        <div className="richescheck-main">
            {
                Array.from(Array( props.gameData.child_expense.quantity)).map((x, index) =>
                    <Checkbox
                        key={index}
                        checked = {true}
                        onChange = { handleChange }
                        color="primary"
                        inputProps={{ 'aria-label': 'primary checkbox' }}
                    />
                )
            }
        </div>
    )
}

const mapDispatchToProps = (dispatch) => ({
    removeChild: () => dispatch(removeChild()),
    updateTotalExpense: () => dispatch(updateTotalExpense()),
    updateCashFlow: () => dispatch(updateCashFlow()),
})

const mapStateToProps = (state) => ({
    gameData: state.gameData
})
 
export default connect(mapStateToProps, mapDispatchToProps)(ChildChecks);