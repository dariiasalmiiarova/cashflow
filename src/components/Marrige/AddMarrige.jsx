import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';

import { addMarrige } from '../../redux/marrige/marrige.action';

const AddMarrige = ({ addMarrige }) => {
    return(
        <Fragment>
            <button type="submit" className="btn btn-prima" onClick={() => addMarrige() }>
                <FormattedMessage  
                    id="btn-game-add"
                    defaultMessage="Dodaj" />
            </button>
        </Fragment>
    )
}

const mapDispatchToProps = (dispatch) => ({
    addMarrige: () => dispatch(addMarrige()),
})


export default connect(null, mapDispatchToProps)(AddMarrige);