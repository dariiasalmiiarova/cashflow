import React, { Fragment } from 'react';
import { connect } from 'react-redux';

import { removeMarrige } from '../../redux/marrige/marrige.action';

import Checkbox from '@material-ui/core/Checkbox';


const MarrigeCheck = ({ marrige, removeMarrige }) => {
    return(
        <Fragment>
            {
                <Checkbox
                    key={marrige.marrige}
                    checked = {marrige.marrige}
                    onChange = {() => removeMarrige()}
                    color="primary"
                    inputProps={{ 'aria-label': 'primary checkbox' }}
                />
            }
        </Fragment>
    )
}

const mapDispatchToProps = (dispatch) => ({
    removeMarrige: () => dispatch(removeMarrige()),
})

const mapStateToProps = (state) => ({
    marrige: state.marrige
})

export default connect(mapStateToProps, mapDispatchToProps)(MarrigeCheck);