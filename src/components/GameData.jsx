import Save from './Save';
import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { auth } from '../services/firebase';

import { FormattedMessage } from 'react-intl';
import { quitJob, updateCashFlow } from '../redux/gameData/gamedata.action';

const GameData = ({ gameData, quitJob, updateCashFlow}) => {
    
    return (
        <Fragment>
            <div className="data-head">
                <div className="main-info">
                    <p className="mb-0 name">{gameData.name}</p>
                    <p className="mb-0 profession">{gameData.profession}</p>
                    {
                        auth().currentUser ? <Save /> : null
                    }
                </div>
            </div>
            <div className="data-main">
                <div className="job">
                    <p className="data-item">
                        <b><FormattedMessage  
                                id="active_incomes"
                                defaultMessage="Dochód aktywny: " /></b>
                        {gameData.active_incomes}
                    </p>
                    {
                       gameData.active_incomes !== 0 ?
                            <button  className="btn btn button-link button-game" onClick={() => { quitJob(); updateCashFlow(); }}>      
                                <FormattedMessage  
                                id="btn-quit-job"
                                defaultMessage="Rzuć pracę" /></button>
                        : null
                    }
                </div>
                <p className="data-item">
                    <b><FormattedMessage  
                        id="passive_incomes"
                        defaultMessage="Dochód pasywny: " /></b>
                    {gameData.passive_incomes}
                </p>
                <p className="data-item">
                    <b><FormattedMessage  
                        id="all_incomes"
                        defaultMessage="Całkowity przychód: " /></b>
                        {gameData.all_incomes}
                </p>
                <p className="data-item">
                    <b><FormattedMessage  
                        id="total_expense"
                        defaultMessage="Całkowity wydatek: " /></b>
                        {gameData.total_expense}
                </p>
                <p className="data-item">
                    <b>Cash flow:</b> {gameData.cash_flow}
                </p>
            </div>
        </Fragment>
    )
}

const mapDispatchToProps = (dispatch) => ({
    quitJob: () => dispatch(quitJob()),
    updateCashFlow: () => dispatch(updateCashFlow()), 
})

const mapStateToProps = (state) => ({
    gameData: state.gameData
})

export default connect(mapStateToProps, mapDispatchToProps)(GameData);