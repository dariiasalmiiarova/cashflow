import React from 'react';
import { connect } from 'react-redux';
import { addShare, deleteShare } from '../../redux/shares/shares.action';


import { FormattedMessage } from 'react-intl';

import TextField from '@material-ui/core/TextField';

class SharesAdd extends React.Component {
    constructor(){
        super();
        this.state={
            key:'',
            quantity: '',
            price: ''
        }
    }
    handleG = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    handleSubmit = e => {
        e.preventDefault();
        try{     
            this.props.addShare(this.state.key, this.state.quantity, this.state.price);
        } catch (error){
            console.log(error);
        }
        this.setState({
            key:'',
            quantity:'',
            price:''
        })
    }
    render(){
        return(
            <div className="business-main">
                <form className="form-inline add-values-form" style={{width: 100+'%'}} onSubmit={this.handleSubmit} >
                    <div className="form-group-b">
                        <TextField 
                                id="outlined-basic1" 
                                label={
                                    <FormattedMessage  
                                    id="shares-key-select"
                                    defaultMessage="Typ" />
                                } 
                                value={this.state.key}
                                name='key'
                                type="text"
                                onChange={this.handleG}
                                variant='outlined'
                                margin='normal'
                                required 
                                />
                        <TextField 
                                id="outlined-basic2" 
                                label={
                                    <FormattedMessage  
                                    id="shares-quantity-input"
                                    defaultMessage="Wprowadź ilość" />
                                } 
                                name="quantity" 
                                value={this.state.quantity}
                                onChange={this.handleG}
                                variant='outlined'
                                margin='normal'
                                required 
                                />
                        <TextField 
                            id="outlined-basic3" 
                            label={
                                <FormattedMessage  
                                id="shares-price-input"
                                defaultMessage="Podaj cenę" />
                            } 
                            type='number'
                            name="price" 
                            value={this.state.price}
                            onChange={this.handleG}
                            variant='outlined'
                            margin='normal'
                            required 
                        />        
                    </div>
                    <button type="submit" className="btn btn-prima form-button">
                        <FormattedMessage  
                                id="btn-game-add"
                                defaultMessage="Dodaj" /></button>
                </form>
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => ({
    addShare: (key, quantity, price) => dispatch(addShare(key, quantity, price)),
    deleteShare: (id) => dispatch(deleteShare(id))
})

export default connect(null, mapDispatchToProps)(SharesAdd);