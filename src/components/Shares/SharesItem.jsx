import React, {Fragment} from 'react'
import { connect } from 'react-redux';

import { deleteShare } from '../../redux/shares/shares.action';
import { FormattedMessage } from 'react-intl';


const SharesItem = ({ shares, deleteShare}) => {
    const element = shares.map(item => {
        // let actual = "p-list";
        return(
            <div className="table-responsive-md">
                <table className="table">
                    <thead>
                        <tr>
                            <th scope="col">
                                <FormattedMessage  
                                    id="shares-key-select"
                                    defaultMessage="Typ" /></th>
                            <th scope="col">
                                <FormattedMessage  
                                    id="shares-quantity-title"
                                    defaultMessage="Ilość" /></th>
                            <th scope="col">
                                <FormattedMessage  
                                    id="shares-price-title"
                                    defaultMessage="Cena" /></th>
                            <th scope="col">
                                <FormattedMessage  
                                    id="btn-delete-game"
                                    defaultMessage="Usuń" /></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{ item.key }</td>
                            <td>{ item.quantity } </td>
                            <td>{ item.price  }</td>
                            <td> 
                                <div className="btn-group" role="group" aria-label="Basic example">
                                    <button type="button" className="btn btn-danger" onClick={
                                        () => {deleteShare(item.id)} }><i className="fas fa-trash"></i></button>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        )
    })
    if (shares.length === 0){
        return (
            <div className="empty-div">
                <p className="empty-text">
                    <FormattedMessage  
                        id="shares-empty-p"
                        defaultMessage="Brak akcji" />
                </p>
            </div>
            
        )
    } else{
        return(
            <div className="todo-list-item-main">
                <Fragment>
                    { element }
                </Fragment>
            </div>
        ) 
    }
}

const mapStateToProps = (state) => ({
    shares: state.shares
})

const mapDispatchToProps = dispatch => ({
    deleteShare: id => dispatch(deleteShare(id))
})

export default connect(mapStateToProps, mapDispatchToProps)(SharesItem);
