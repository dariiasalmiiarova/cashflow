import React from 'react';
import { connect } from "react-redux";

import { removeCottageExpense, updateTotalExpense, updateCashFlow } from '../../redux/gameData/gamedata.action';

import Checkbox from '@material-ui/core/Checkbox';
import { FormattedMessage } from 'react-intl';

const CottageChecks = (props) => {
    const handleChange = () => {
        props.removeCottageExpense(props.gameData.cottage_expense.quantity);
        props.updateTotalExpense();
        props.updateCashFlow();
    }
    return(
        <div className="richescheck-main">
            <p className="expense-title">
                <FormattedMessage  
                    id="cottage-title"
                    defaultMessage="Domki" /></p>
            {
                Array.from(Array( props.gameData.cottage_expense.quantity)).map((x, index) =>
                    <Checkbox
                        key={index}
                        checked = {true}
                        onChange = {handleChange}
                        color="primary"
                        inputProps={{ 'aria-label': 'primary checkbox' }}
                    />
                )
            }
        </div>
    )
}

const mapDispatchToProps = (dispatch) => ({
    removeCottageExpense: () => dispatch(removeCottageExpense()),
    updateTotalExpense: () => dispatch(updateTotalExpense()),
    updateCashFlow: () => dispatch(updateCashFlow()),
})

const mapStateToProps = (state) => ({
    gameData: state.gameData
})
 
export default connect(mapStateToProps, mapDispatchToProps)(CottageChecks);