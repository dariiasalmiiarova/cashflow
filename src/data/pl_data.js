const Professions = {
    ratRun: {
        id: 1,
        items: [
            {
                id: 1,
                info: {
                    name: 'Kierownik Sprzedaży',
                    active_incomes: 600,
                    passive_incomes: 0,
                    rental_housting: 100,
                    food_costs: 150,
                    clothing_costs: 50,
                    travel_expenses: 50,
                    phone_costs: 50,
                    total_expense: 400,
                    cash_flow: 200
                }
            },
            {
                id: 3,
                info: {
                    name: 'Zastępca Asystenta',
                    active_incomes: 2500,
                    passive_incomes: 0,
                    rental_housting: 800,
                    food_costs: 300,
                    clothing_costs: 200,
                    travel_expenses: 150,
                    phone_costs: 100,
                    total_expense: 1550,
                    cash_flow: 950
                }
            },
            {
                id: 4,
                info: {
                    name: 'Pracownik ładowarki',
                    active_incomes: 400,
                    passive_incomes: 0,
                    rental_housting: 100,
                    food_costs: 150,
                    clothing_costs: 10,
                    travel_expenses: 20,
                    phone_costs: 10,
                    total_expense: 290,
                    cash_flow: 110
                }
            },
            {
                id: 5,
                info: {
                    name: 'Kierownik działu',
                    active_incomes: 1000,
                    passive_incomes: 0,
                    rental_housting: 450,
                    food_costs: 150,
                    clothing_costs: 50,
                    travel_expenses: 50,
                    phone_costs: 50,
                    total_expense: 750,
                    cash_flow: 250
                }
            },
            {
                id: 6,
                info: {
                    name: 'Operator dźwigu',
                    active_incomes: 600,
                    passive_incomes: 0,
                    rental_housting: 100,
                    food_costs: 150,
                    clothing_costs: 10,
                    travel_expenses: 30,
                    phone_costs: 10,
                    total_expense: 300,
                    cash_flow: 300
                }
            },
            {
                id: 7,
                info: {
                    name: 'Taksówkarz w czyimś samochodzie',
                    active_incomes: 1000,
                    passive_incomes: 0,
                    rental_housting: 300,
                    food_costs: 150,
                    clothing_costs: 20,
                    travel_expenses: 0,
                    phone_costs: 10,
                    total_expense: 480,
                    cash_flow: 520
                }
            },
            {
                id: 8,
                info: {
                    name: 'Chirurg',
                    active_incomes: 1500,
                    passive_incomes: 0,
                    rental_housting: 500,
                    food_costs: 250,
                    clothing_costs: 50,
                    travel_expenses: 30,
                    phone_costs: 20,
                    total_expense: 850,
                    cash_flow: 650
                }
            },{
                id: 9,
                info: {
                    name: 'Prawnik',
                    active_incomes: 2000,
                    passive_incomes: 0,
                    rental_housting: 500,
                    food_costs: 300,
                    clothing_costs: 100,
                    travel_expenses: 100,
                    phone_costs: 100,
                    total_expense: 1100,
                    cash_flow: 900
                }
            },
            {
                id: 10,
                info: {
                    name: 'Redaktor',
                    active_incomes: 1500,
                    passive_incomes: 0,
                    rental_housting: 500,
                    food_costs: 150,
                    clothing_costs: 50,
                    travel_expenses: 50,
                    phone_costs: 50,
                    total_expense: 900,
                    cash_flow: 600
                }
            },
            {
                id: 11,
                info: {
                    name: 'Dzielnicowy',
                    active_incomes: 400,
                    passive_incomes: 0,
                    rental_housting: 100,
                    food_costs: 150,
                    clothing_costs: 10,
                    travel_expenses: 20,
                    phone_costs: 10,
                    total_expense: 290,
                    cash_flow: 110
                }
            },
            {
                id: 12,
                info: {
                    name: 'Dentysta',
                    active_incomes: 800,
                    passive_incomes: 0,
                    rental_housting: 300,
                    food_costs: 200,
                    clothing_costs: 20,
                    travel_expenses: 30,
                    phone_costs: 20,
                    total_expense: 570,
                    cash_flow: 230
                }
            },
            {
                id: 13,
                info: {
                    name: 'Konsultant prawny',
                    active_incomes: 1000,
                    passive_incomes: 0,
                    rental_housting: 500,
                    food_costs: 150,
                    clothing_costs: 50,
                    travel_expenses: 50,
                    phone_costs: 50,
                    total_expense: 800,
                    cash_flow: 200
                }
            },
            {
                id: 14,
                info: {
                    name: 'Top manager w międzynarodowej firmie',
                    active_incomes: 2500,
                    passive_incomes: 0,
                    rental_housting: 800,
                    food_costs: 400,
                    clothing_costs: 100,
                    travel_expenses: 150,
                    phone_costs: 100,
                    total_expense: 1550,
                    cash_flow: 950
                }
            },
            {
                id: 15,
                info: {
                    name: 'Księgowy',
                    active_incomes: 800,
                    passive_incomes: 0,
                    rental_housting: 300,
                    food_costs: 150,
                    clothing_costs: 20,
                    travel_expenses: 30,
                    phone_costs: 10,
                    total_expense: 510,
                    cash_flow: 290
                }
            },
            {
                id: 16,
                info: {
                    name: 'Magazynier',
                    active_incomes: 500,
                    passive_incomes: 0,
                    rental_housting: 100,
                    food_costs: 150,
                    clothing_costs: 20,
                    travel_expenses: 20,
                    phone_costs: 10,
                    total_expense: 300,
                    cash_flow: 200
                }
            },
            {
                id: 17,
                info: {
                    name: 'Majster',
                    active_incomes: 800,
                    passive_incomes: 0,
                    rental_housting: 300,
                    food_costs: 150,
                    clothing_costs: 20,
                    travel_expenses: 30,
                    phone_costs: 10,
                    total_expense: 510,
                    cash_flow: 290
                }
            },
            {
                id: 18,
                info: {
                    name: 'Agent reklamowy',
                    active_incomes: 500,
                    passive_incomes: 0,
                    rental_housting: 100,
                    food_costs: 150,
                    clothing_costs: 20,
                    travel_expenses: 30,
                    phone_costs: 10,
                    total_expense: 310,
                    cash_flow: 190
                }
            },
            {
                id: 19,
                info: {
                    name: 'Projektant',
                    active_incomes: 800,
                    passive_incomes: 0,
                    rental_housting: 300,
                    food_costs: 150,
                    clothing_costs: 30,
                    travel_expenses: 30,
                    phone_costs: 40,
                    total_expense: 550,
                    cash_flow: 250
                }
            },
            {
                id: 20,
                info: {
                    name: 'Menedżer ds. Reklamy',
                    active_incomes: 1000,
                    passive_incomes: 0,
                    rental_housting: 300,
                    food_costs: 150,
                    clothing_costs: 50,
                    travel_expenses: 50,
                    phone_costs: 50,
                    total_expense: 600,
                    cash_flow: 400
                }
            },
            {
                id: 21,
                info: {
                    name: 'Dyrektor czyjejś firmy',
                    active_incomes: 2000,
                    passive_incomes: 0,
                    rental_housting: 500,
                    food_costs: 300,
                    clothing_costs: 100,
                    travel_expenses: 150,
                    phone_costs: 100,
                    total_expense: 1550,
                    cash_flow: 850
                }
            },
            {
                id: 22,
                info: {
                    name: 'Kierowca ciężarówki',
                    active_incomes: 800,
                    passive_incomes: 0,
                    rental_housting: 300,
                    food_costs: 150,
                    clothing_costs: 20,
                    travel_expenses: 0,
                    phone_costs: 10,
                    total_expense: 480,
                    cash_flow: 320
                }
            },
            {
                id: 23,
                info: {
                    name: 'Wykładowca w Instytucie',
                    active_incomes: 500,
                    passive_incomes: 0,
                    rental_housting: 100,
                    food_costs: 150,
                    clothing_costs: 50,
                    travel_expenses: 30,
                    phone_costs: 10,
                    total_expense: 340,
                    cash_flow: 160
                }
            },
            {
                id: 24,
                info: {
                    name: 'Korespondent',
                    active_incomes: 500,
                    passive_incomes: 0,
                    rental_housting: 100,
                    food_costs: 150,
                    clothing_costs: 20,
                    travel_expenses: 50,
                    phone_costs: 20,
                    total_expense: 340,
                    cash_flow: 160
                }
            },
            {
                id: 25,
                info: {
                    name: 'Stróż',
                    active_incomes: 300,
                    passive_incomes: 0,
                    rental_housting: 100,
                    food_costs: 100,
                    clothing_costs: 10,
                    travel_expenses: 20,
                    phone_costs: 0,
                    total_expense: 230,
                    cash_flow: 70
                }
            },
            {
                id: 26,
                info: {
                    name: 'Urzędnik podatkowy',
                    active_incomes: 1500,
                    passive_incomes: 0,
                    rental_housting: 500,
                    food_costs: 300,
                    clothing_costs: 100,
                    travel_expenses: 100,
                    phone_costs: 50,
                    total_expense: 1050,
                    cash_flow: 450
                }
            },
            {
                id: 27,
                info: {
                    name: 'Asystent sprzedaży',
                    active_incomes: 500,
                    passive_incomes: 0,
                    rental_housting: 100,
                    food_costs: 150,
                    clothing_costs: 50,
                    travel_expenses: 30,
                    phone_costs: 10,
                    total_expense: 340,
                    cash_flow: 160
                }
            },
            {
                id: 28,
                info: {
                    name: 'Sędzia śledczy',
                    active_incomes: 1500,
                    passive_incomes: 0,
                    rental_housting: 500,
                    food_costs: 300,
                    clothing_costs: 30,
                    travel_expenses: 30,
                    phone_costs: 40,
                    total_expense: 900,
                    cash_flow: 600
                }
            },
            {
                id: 29,
                info: {
                    name: 'Lekarz w szpitalu',
                    active_incomes: 600,
                    passive_incomes: 0,
                    rental_housting: 200,
                    food_costs: 150,
                    clothing_costs: 20,
                    travel_expenses: 30,
                    phone_costs: 10,
                    total_expense: 410,
                    cash_flow: 190
                }
            },
        ]
    },
    cashflow: {
        id: 2,
        items: [
            {
                id: 30,
                info: {
                    name: 'Pilot lini lotniczych',
                    active_incomes: 9500,
                    passive_incomes: 0,
                    rental_housting: 1330,
                    food_costs: 2350,
                    clothing_costs: 660,
                    travel_expenses: 300,
                    phone_costs: 50,
                    total_expense: 4690,
                    cash_flow: 4810
                }
            },
            {
                id: 31,
                info: {
                    name: 'Menedżer biznesu',
                    active_incomes: 4600,
                    passive_incomes: 0,
                    rental_housting: 700,
                    food_costs: 910,
                    clothing_costs: 90,
                    travel_expenses: 120,
                    phone_costs: 60,
                    total_expense: 1880,
                    cash_flow: 2720
                }
            },
            {
                id: 32,
                info: {
                    name: 'Lekarz',
                    active_incomes: 13200,
                    passive_incomes: 0,
                    rental_housting: 2900,
                    food_costs: 4800,
                    clothing_costs: 1500,
                    travel_expenses: 380,
                    phone_costs: 50,
                    total_expense: 9630,
                    cash_flow: 3570
                }
            },
            {
                id: 33,
                info: {
                    name: 'Inżynier',
                    active_incomes: 4900,
                    passive_incomes: 0,
                    rental_housting: 1750,
                    food_costs: 1090,
                    clothing_costs: 160,
                    travel_expenses: 140,
                    phone_costs: 50,
                    total_expense: 3190,
                    cash_flow: 1760
                }
            },
            {
                id: 34,
                info: {
                    name: 'Dozorca',
                    active_incomes: 1600,
                    passive_incomes: 0,
                    rental_housting: 480,
                    food_costs: 300,
                    clothing_costs: 60,
                    travel_expenses: 60,
                    phone_costs: 50,
                    total_expense: 950,
                    cash_flow: 650
                }
            },
            {
                id: 35,
                info: {
                    name: 'Prawnik',
                    active_incomes: 7500,
                    passive_incomes: 0,
                    rental_housting: 2930,
                    food_costs: 1650,
                    clothing_costs: 390,
                    travel_expenses: 220,
                    phone_costs: 50,
                    total_expense: 5240,
                    cash_flow: 2260
                }
            },
            {
                id: 36,
                info: {
                    name: 'Mechanik',
                    active_incomes: 2000,
                    passive_incomes: 0,
                    rental_housting: 660,
                    food_costs: 450,
                    clothing_costs: 60,
                    travel_expenses: 60,
                    phone_costs: 50,
                    total_expense: 1280,
                    cash_flow: 720
                }
            },
            {
                id: 37,
                info: {
                    name: 'Pielęgniarka',
                    active_incomes: 3100,
                    passive_incomes: 0,
                    rental_housting: 1300,
                    food_costs: 710,
                    clothing_costs: 90,
                    travel_expenses: 100,
                    phone_costs: 50,
                    total_expense: 2250,
                    cash_flow: 850
                }
            },
            {
                id: 38,
                info: {
                    name: 'Policjant',
                    active_incomes: 3000,
                    passive_incomes: 0,
                    rental_housting: 980,
                    food_costs: 690,
                    clothing_costs: 60,
                    travel_expenses: 100,
                    phone_costs: 50,
                    total_expense: 1880,
                    cash_flow: 1120
                }
            },
            {
                id: 39,
                info: {
                    name: 'Sekretarz',
                    active_incomes: 2500,
                    passive_incomes: 0,
                    rental_housting: 860,
                    food_costs: 570,
                    clothing_costs: 60,
                    travel_expenses: 80,
                    phone_costs: 50,
                    total_expense: 1620,
                    cash_flow: 880
                }
            },
            {
                id: 40,
                info: {
                    name: 'Nauczyciel',
                    active_incomes: 3300,
                    passive_incomes: 0,
                    rental_housting: 1190,
                    food_costs: 760,
                    clothing_costs: 90,
                    travel_expenses: 100,
                    phone_costs: 50,
                    total_expense: 2190,
                    cash_flow: 1110
                }
            },
            {
                id: 41,
                info: {
                    name: 'Kierowca ciężarówki',
                    active_incomes: 2500,
                    passive_incomes: 0,
                    rental_housting: 860,
                    food_costs: 570,
                    clothing_costs: 60,
                    travel_expenses: 80,
                    phone_costs: 50,
                    total_expense: 1620,
                    cash_flow: 880
                }
            }
        ]
    }
}

export default Professions;