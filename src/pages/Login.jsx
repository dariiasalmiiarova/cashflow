import React from 'react';
import { Link } from 'react-router-dom';
import { signin } from '../services/auth';
import TextField from '@material-ui/core/TextField';

import { FormattedMessage } from 'react-intl';

export default class Login extends React.Component{
    constructor(){
        super();
        this.state={
            email: '',
            password: '',
            error: null
        };
    }
    handleChange = (e) => {
        this.setState({
            //присваиваем соответствующему полю занчение с ввода
            [e.target.name]: e.target.value
        })
    }
    handleSubmit = async e => {
        e.preventDefault();
        this.setState({ error: '' });
        try{
            await signin(this.state.email, this.state.password);
        } catch(error){
            this.setState({ error: error.message });
        }
    }
    render(){
        return(
            <div className="container">
                <form className="mt-5 py-5 px-5" onSubmit={this.handleSubmit}>
                    <h1>
                    <FormattedMessage  
                        id="sign-in-text"
                        defaultMessage="Login to" />
                        <Link className="title" to="/">CashFlow</Link>
                    </h1>
                    <p className="lead">
                        <FormattedMessage  
                            id="auth-text"
                            defaultMessage="Fill in the form below" />
                    </p>
                    <div className="form-group pt-5">
                        <TextField 
                            id="outlined-basic" 
                            label={
                            <FormattedMessage  
                                id="email-input"
                                defaultMessage="Email" />
                            } 
                            name='email'
                            type='email'
                            onChange={this.handleChange}
                            value={this.state.name}
                            variant='outlined'
                            margin='normal'
                            required 
                            />
                        <TextField 
                            id="outlined-basic" 
                            label={
                            <FormattedMessage  
                                id="password-input"
                                defaultMessage="Password" />
                            } 
                            name='password'
                            type='password'
                            onChange={this.handleChange}
                            value={this.state.password}
                            variant='outlined'
                            margin='normal'
                            required 
                         />
                    </div>
                    <div className="form-button">
                        { this.state.error ? <p className="text-danger">{ this.state.error }</p> : null }
                        <button className="btn btn-prima rounded-pill px-5">
                            <FormattedMessage  
                                id="btn-sign-in"
                                defaultMessage="Login" /></button>
                    </div>
                </form>
                <hr/>
                <p><FormattedMessage  
                    id="login-text-below"
                    defaultMessage="Login" /> 
                <Link to="/signup" className="title">
                    <FormattedMessage  
                        id="btn-sign-up"
                        defaultMessage="Sign up" />
                 </Link></p>
            </div>
        )
    }
}