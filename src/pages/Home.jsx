import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { addGameData, addCarExpense, addFlatExpense, addCottageExpense, addYachtExpense, addPlaneExpense, addChild, addCreditSum } from '../redux/gameData/gamedata.action';
import { addRichesArray } from '../redux/riches/riches.action';
import { addArrayDebts } from '../redux/debts/debts.action';
import { addBusinessArray } from '../redux/business/business.action';
import { addCreditsArray } from '../redux/credits/credits.action';
import { addShare } from '../redux/shares/shares.action';
import { addMarrigeArray } from '../redux/marrige/marrige.action';

import { FormattedMessage } from 'react-intl';
import { auth, db } from '../services/firebase';
class Home extends React.Component {

    constructor() {
        super();
        this.state ={
            user: auth().currentUser,
            games: [],
        }
    }
    
    componentDidMount() {
        if(this.state.user){
            db.ref(`all_games/${this.state.user.uid}`).on("value", snapshot => {
                let allGames = [];
                snapshot.forEach(snap => {
                  allGames.push(snap.val());
                });
                this.setState({ games: allGames });
              });
        } else {
            return null
        }
        
    }
    
    render(){
        return(
            <div className="container">
                <div className="start-sections">
                    <Link className="btn button-link" to="/createnewgame">
                        <FormattedMessage  
                            id="btn-start-game"
                            defaultMessage="Start new game"/></Link>
                </div>
                <section className="games-section">
                    { auth().currentUser ? 
                            <div className="card">
                                <h5 className="card-header">
                                    <FormattedMessage  
                                        id="name-of-section"
                                        defaultMessage="Gry"/></h5>
                                <div className="card-body">
                                {this.state.games.length === 0 ? <div><p>There is no started games yet.</p></div> : 
                                    this.state.games.map((item) =>(
                                            <div className="content" key = {item.game_id}>
                                                <div className="titles-group">
                                                    <h5 className="card-title">{item.gameData.profession}</h5>
                                                    <p className="card-text">+{item.gameData.all_incomes}</p>
                                                </div>
                                                <div className="game-cart-buttons">
                                                <button className="btn button-link button-choise" onClick={() => { 
                                                    this.props.addGameData(item.game_id, item.gameData.name, item.gameData.profession, item.gameData.active_incomes, item.gameData.passive_incomes, item.gameData.rental_housting, item.gameData.food_costs, item.gameData.clothing_costs, item.gameData.travel_expenses, item.gameData.phone_costs, item.gameData.total_expense, item.gameData.cash_flow, item.gameData.item); 
                                                    this.props.addCarExpense(item.gameData.car_expense.quantity);   
                                                    this.props.addFlatExpense(item.gameData.flat_expense.quantity); 
                                                    this.props.addCottageExpense(item.gameData.cottage_expense.quantity); 
                                                    this.props.addYachtExpense(item.gameData.yacht_expense.quantity); 
                                                    this.props.addPlaneExpense(item.gameData.plane_expense.quantity); 
                                                    this.props.addChild(item.gameData.child_expense.quantity); 
                                                    this.props.addRich(item.riches || []); 
                                                    this.props.addDebts(item.debts || []); 
                                                    this.props.addCredit(item.credits || []); 
                                                    this.props.addCreditSum(item.gameData.credits.sum); 
                                                    this.props.addBusiness(item.business || []);
                                                    this.props.addMarrigeArray(item.marrige);
                                                    this.props.history.push('/game'); }} 
                                                >
                                                    <FormattedMessage  
                                                            id="btn-to-game"
                                                            defaultMessage="Grać"/></button>
                                                    <button className="btn button-link button-choise button-delete" onClick={() => {
                                                        db.ref(`all_games/${this.state.user.uid}/${item.game_id}`).remove()
                                                    }}>
                                                        <FormattedMessage  
                                                            id="btn-delete-game"
                                                            defaultMessage="Usuń"/></button>
                                                </div>
                                                <div className="dropdown-divider"></div>
                                            </div>
                                        ))
                                } 
                                </div> 
                            </div> : null
                    }
                </section>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => ({
    addGameData: (game_id, name, profession, active_incomes, passive_incomes, rental_housting, food_costs, clothing_costs, travel_expenses, phone_costs,total_expense, cash_flow, item) => dispatch(addGameData(game_id, name, profession, active_incomes, passive_incomes, rental_housting, food_costs, clothing_costs, travel_expenses, phone_costs,total_expense, cash_flow, item)),
    addCarExpense: (quantity) => dispatch(addCarExpense(quantity)),
    addFlatExpense: (quantity) => dispatch(addFlatExpense(quantity)),
    addCottageExpense: (quantity) => dispatch(addCottageExpense(quantity)),
    addYachtExpense: (quantity) => dispatch(addYachtExpense(quantity)),
    addPlaneExpense: (quantity) => dispatch(addPlaneExpense(quantity)),
    addChild: (quantity) => dispatch(addChild(quantity)),
    addRich: (riches) => dispatch(addRichesArray(riches)),
    addDebts: (debts) => dispatch(addArrayDebts(debts)),
    addBusiness: (business) => dispatch(addBusinessArray(business)),
    addCredit: (credits) => dispatch(addCreditsArray(credits)),
    addShare: (key, quantity, price) => dispatch(addShare(key, quantity, price)),
    addCreditSum: (sum) => dispatch(addCreditSum(sum)),
    addMarrigeArray: (marrige) => dispatch(addMarrigeArray(marrige))
})

export default connect(null, mapDispatchToProps)(Home);