import React from 'react';
import { Link } from 'react-router-dom';
import { signup } from '../services/auth';
import { FormattedMessage } from 'react-intl';
import TextField from '@material-ui/core/TextField';

export default class SignUp extends React.Component {
    constructor(){
        super();
        this.state={
            error: null,
            email: '',
            password: '',
            confirmPassword:'',
        };
    }

    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value 
        });
    }

    handleSubmit = async event => {
        event.preventDefault();
        this.setState({ error: '' });
        if(this.state.password !== this.state.confirmPassword){
            alert("passwords don't match")
            return;
        } 
        try{
            await signup(this.state.email, this.state.password);
        } catch(error) {
            this.setState({ error: error.message });
        }
    }

    render(){
        return(
            <div className="container">
                <form className="form-group mt-5" autoComplete="off" onSubmit={this.handleSubmit}>
                    <h1 className="text-st">
                    <FormattedMessage  
                        id="sihn-up-text"
                        defaultMessage="Sign Up to " />
                        <Link className="title" to="/">CashFlow</Link>
                    </h1>
                    <p className="lead">
                        <FormattedMessage  
                            id="auth-text"
                            defaultMessage="Fill in the below form" />
                    </p>
                    <div className="form-group-b pt-5">
                    <TextField 
                            id="outlined-basic" 
                            label={
                            <FormattedMessage  
                                id="email-input"
                                defaultMessage="Email" />
                            } 
                            name='email'
                            type='email'
                            onChange={this.handleChange}
                            value={this.state.name}
                            variant='outlined'
                            margin='normal'
                            required 
                            />
                    <TextField 
                            id="outlined-basic" 
                            label={
                            <FormattedMessage  
                                id="password-input"
                                defaultMessage="Password" />
                            } 
                            name='password'
                            type='password'
                            onChange={this.handleChange}
                            value={this.state.password}
                            variant='outlined'
                            margin='normal'
                            required 
                            />
                    <TextField 
                            id="outlined-basic" 
                            label={
                            <FormattedMessage  
                                id="confirm-password"
                                defaultMessage="Confirm password" />
                            } 
                            name='confirmPassword'
                            type='password'
                            onChange={this.handleChange}
                            value={this.state.confirmPassword}
                            variant='outlined'
                            margin='normal'
                            required 
                            />
                    </div>
                    <div className="form-button mt-3">
                        {this.state.error ? <p className="text-danger">{ this.state.error }</p> : null }
                        <button className="btn btn-prima rounded-pill px-5">
                        <FormattedMessage  
                                id="btn-sign-up"
                                defaultMessage="Sign up" /></button>
                    </div>
                    <hr/>
                    <p className="text-st">
                        <FormattedMessage  
                            id="sign-up-text-below"
                            defaultMessage="Already have an account? " />
                        <Link className="title" to="/login">Login</Link> </p>
                </form>
            </div>
        )
    }
}