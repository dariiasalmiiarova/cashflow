import React from 'react';
import { connect } from 'react-redux';

import { updateProf } from '../redux/profession/prof.action';
import { addGameData, addDefaultData } from '../redux/gameData/gamedata.action';
import { FormattedMessage } from 'react-intl';
import TextField from '@material-ui/core/TextField';

import ProfessionsPL from '../data/pl_data.js';
import ProfessionsRU from '../data/ru_data.js';

import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

class CreateNewGame extends React.Component{
    constructor(){
        super();
        this.state = {
            profession: ProfessionsPL,
            choice: '',
            item: {},
            version: 'ratRun',
            name: '',
            game_id: `game-${Date.now()}`,
        };
    }

    componentDidMount(){
        this.props.locale === "pl" ? this.setState({ profession: ProfessionsPL }) : this.setState({ profession: ProfessionsRU })
    }

    componentDidUpdate(prevProps) {
        if ( this.props.locale !== prevProps.locale ) {
            this.props.locale === "pl" ? this.setState({ profession: ProfessionsPL }) : this.setState({ profession: ProfessionsRU })
        }
    }

    handleG = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    handleSubmit(path){
        return async e => {
            e.preventDefault();
            try{
                if(this.state.choice !== null) {
                    path.filter((it) => {
                        return it.id === this.state.choice;
                        // eslint-disable-next-line
                    }).map(item => {
                        // this.props.addDefaultData(item)
                        this.props.addGameData(this.state.game_id, this.state.name, item.info.name, item.info.active_incomes, item.info.passive_incomes, item.info.rental_housting, item.info.food_costs, item.info.clothing_costs, item.info.travel_expenses, item.info.phone_costs, item.info.total_expense, item.info.cash_flow, item);
                    })
                };          
                this.props.updateProf(this.state.profession);
                this.props.history.push('/game');
            } catch (error){
                console.log(error);
            }
        }
    }
    
    render(){
        const path = this.state.version === "ratRun" ?  this.state.profession.ratRun.items : this.state.profession.cashflow.items
        return(
            <form className="container mt-5 py-5 px-5 form-create-game" onSubmit={this.handleSubmit(path)}>
                <div className="form-group-b">
                    <TextField 
                        id="outlined-basic" 
                        label={
                            <FormattedMessage                                      
                                id="input-name"
                                defaultMessage="Name" />
                            } 
                        name='name'
                        type='text'
                        onChange={this.handleG}
                        value={this.state.name}
                        variant='outlined'
                        margin='normal'
                        required 
                    />
                </div>
                <div className="select-div">
                    <FormControl className="mr-5 mt-5 form-select">
                        <Select
                            labelId="demo-simple-select-labell"
                            id="demo-simple-select"
                            className="select-input"
                            value={this.state.version}
                            name='version'
                            onChange={this.handleG}
                            label="Wersja"
                            required
                        >
                        <MenuItem value=""><em>
                            <FormattedMessage  
                            id="select-defaul-version"
                            defaultMessage="Wersja" />
                            </em></MenuItem>
                        <MenuItem value="ratRun">
                            <FormattedMessage  
                                id="select-rat"
                                defaultMessage="Wyścig szczurów" /></MenuItem>
                        <MenuItem value="cashflow">
                            <FormattedMessage  
                                id="select-cashflow"
                                defaultMessage="Cashflow" /></MenuItem>
                        </Select>
                    </FormControl>

                    <FormControl className="mt-5 form-select">
                        <Select
                            id="demo-simple-select"
                            value={this.state.choice}
                            name="choice" 
                            onChange={this.handleG}
                            required
                            displayEmpty
                            className='select-input'>
                            <MenuItem value="">
                                <em>
                                    <FormattedMessage  
                                        id="select-profession"
                                        defaultMessage="Zawód" />
                                    </em>
                            </MenuItem>
                            {path.map(item => (
                                <MenuItem key={item.id} value={item.id}>{ item.info.name }</MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                </div>
                <div className="form-group-button">
                    <button className="btn button-link btn-start">
                        <FormattedMessage  
                            id="btn-start-game"
                            defaultMessage="Zacząć nową grę" /></button>
                </div>
            </form>
        )
    }
}

const mapStateToProps = (state) => ({
    locale: state.local
})

const mapDispatchToProps = (dispatch) => {
    return{
        updateProf: (collectionMap) => dispatch(updateProf(collectionMap)),
        addGameData: (game_id, name, profession, active_incomes, passive_incomes, rental_housting, food_costs, clothing_costs, travel_expenses, phone_costs,total_expense, cash_flow, item) => dispatch(addGameData(game_id, name, profession, active_incomes, passive_incomes, rental_housting, food_costs, clothing_costs, travel_expenses, phone_costs,total_expense, cash_flow, item)),
        addDefaultData: (item) => dispatch(addDefaultData(item)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateNewGame);