import React, { useState } from 'react';
import { IntlProvider } from 'react-intl';
import { connect } from 'react-redux';

import { localeAction } from '../redux/locale/local.action';
import Russian from './ru-Ru.json';
import Polish from './pl-Pl.json';

export const Context = React.createContext();
const local = 'pl';

let lang;
if(local === 'ru'){
    lang = Russian
} else{
    lang = Polish
}

const Translation = (props) =>{
    const [ locale, setLocale ] = useState(local);
    const [ messages, setMessages ] = useState(lang);
    
    props.localeAction(locale)
    
    function selectLangRussian(){
        setLocale("ru");
        setMessages(Russian);
    }
    function selectLangPolish(){
        setLocale("pl");
        setMessages(Polish);
    }
        return(
            <Context.Provider value={{ locale, selectLangRussian, selectLangPolish }}>
                <IntlProvider messages={messages} locale={locale}>
                    {props.children}
                </IntlProvider>
            </Context.Provider>
        )
}

const mapDispatchToProps = (dispatch) => ({
    localeAction: (locale) => dispatch(localeAction(locale))
})

export default connect(null, mapDispatchToProps)(Translation);