import firebase from 'firebase';

var firebaseConfig = {
    apiKey: "AIzaSyCGz210DPEmgEGmw9X_dxpUjRmeFe6FuHQ",
    authDomain: "cashflow-4821b.firebaseapp.com",
    databaseURL: "https://cashflow-4821b.firebaseio.com",
    projectId: "cashflow-4821b",
    storageBucket: "cashflow-4821b.appspot.com",
    messagingSenderId: "937678440608",
    appId: "1:937678440608:web:fa0dff706f88bbb38121c5"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

  export const auth = firebase.auth;

  export const db = firebase.database();
  // export const db = firebase.firestore();